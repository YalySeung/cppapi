﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// 20200502.rc에서 사용되고 있습니다.
//
#define IDC_MYICON                      2
#define IDD_MY20200502_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MY20200502                  107
#define IDI_SMALL                       108
#define IDC_MY20200502                  109
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     129
#define IDD_DIALOG1                     130
#define IDD_DIALOG2                     131
#define IDC_BUTTON1                     1000
#define ID_Hor2                         32779
#define ID_Hor3                         32780
#define ID_Hor4                         32781
#define ID_Ver2                         32782
#define ID_Ver3                         32783
#define ID_Ver4                         32784
#define ID_32785                        32785
#define IDM_OPEN                        32786
#define ID_32787                        32787
#define ID_32788                        32788
#define IDM_Modal                       32789
#define IDM_MODALESS                    32790
#define IDM_MODAL_DLG                   32791
#define IDM_MODALESS_DLG                32792
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32793
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
