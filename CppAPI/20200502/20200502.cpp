﻿// 20200502.cpp : 애플리케이션에 대한 진입점을 정의합니다.
//
#pragma once
#include "framework.h"
#include "BasicImageDraw.h"
#include "20200502.h"

#define MAX_LOADSTRING 100

// 전역 변수:
HINSTANCE hInst;                                // 현재 인스턴스입니다.
WCHAR szTitle[MAX_LOADSTRING];                  // 제목 표시줄 텍스트입니다.
WCHAR szWindowClass[MAX_LOADSTRING];            // 기본 창 클래스 이름입니다.

// 이 코드 모듈에 포함된 함수의 선언을 전달합니다:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    DlgProc(HWND, UINT, WPARAM, LPARAM); //선언부에서는 자료형만 들어가도 됩니다.

int HorPieceCnt = 1;
int VerPieceCnt = 1;
int horGap = 3;
int verGap = 3;

bool isTransParentMode;
int alpha = 255 ;

TCHAR _szBuffer[MAX_PATH] = { 0, };
TCHAR _szFullPath[MAX_PATH] = { 0, };
TCHAR _szItems[MAX_ITEM_COUNT][MAX_PATH] = { L"한식", L"중식", L"일식", L"프랑스식", L"양식" };

BasicImageDraw* _pBasicDraw = NULL;
HWND _btn1hWnd;
HWND _btn2hWnd;
HWND _btn3hWnd; //추가
HWND _btn4hWnd; //삭제
HWND _ckb1hWnd;
HWND _edit1hWnd;
HWND _edit2hWnd;
HWND _edit3hWnd;
HWND _list1hWnd;
HWND _combo1hWnd;
HWND _scroll1hWnd;

TCHAR button1Txt1[MAX_LOADSTRING] = L"Split Horizontal";
TCHAR button2Txt1[MAX_LOADSTRING] = L"Split Vertical";
TCHAR button1Txt2[MAX_LOADSTRING] = L"Increase Alpha";
TCHAR button2Txt2[MAX_LOADSTRING] = L"Decrease Alpha";
TCHAR button3Txt1[MAX_LOADSTRING] = L"삭제";
TCHAR button4Txt1[MAX_LOADSTRING] = L"추가";


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 여기에 코드를 입력합니다.

    // 전역 문자열을 초기화합니다.
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY20200502, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 애플리케이션 초기화를 수행합니다:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY20200502));

    MSG msg;

    // 기본 메시지 루프입니다:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  함수: MyRegisterClass()
//
//  용도: 창 클래스를 등록합니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY20200502));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MY20200502);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   용도: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   주석:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, CW_USEDEFAULT, 1000, 1000, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  용도: 주 창의 메시지를 처리합니다.
//
//  WM_COMMAND  - 애플리케이션 메뉴를 처리합니다.
//  WM_PAINT    - 주 창을 그립니다.
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
        _pBasicDraw = new BasicImageDraw(hInst, hWnd, GetDC(hWnd));
        _btn1hWnd = CreateWindow(L"Button", button1Txt1, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 20, 20, 120, 25, hWnd, (HMENU)IDX_BUTTON_01, hInst, NULL);
        _btn2hWnd = CreateWindow(L"Button", button2Txt1, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 20, 45, 120, 25, hWnd, (HMENU)IDX_BUTTON_02, hInst, NULL);
        _btn3hWnd = CreateWindow(L"Button", button3Txt1, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 160, 180, 80, 25, hWnd, (HMENU)IDX_BUTTON_03, hInst, NULL);
        _btn4hWnd = CreateWindow(L"Button", button4Txt1, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 160, 400, 80, 25, hWnd, (HMENU)IDX_BUTTON_04, hInst, NULL);
        _ckb1hWnd = CreateWindow(L"Button", L"TransParent", WS_CHILD | WS_VISIBLE | BS_CHECKBOX, 20, 80, 120, 25, hWnd, (HMENU)IDX_CHECKBOX_01, hInst, NULL);
        _edit1hWnd = CreateWindow(L"Edit", L"안녕하세요", WS_BORDER | WS_VISIBLE | WS_CHILD | ES_MULTILINE, 20, 120, 200, 50, hWnd, (HMENU)IDX_EDITBOX_01, hInst, NULL);
        _edit2hWnd = CreateWindow(L"Edit", L"추가할 항목 입력", WS_BORDER | WS_VISIBLE | WS_CHILD | ES_AUTOVSCROLL, 20, 400, 150, 25, hWnd, (HMENU)IDX_EDITBOX_02, hInst, NULL);
        _edit3hWnd = CreateWindow(L"Edit", L"100", WS_BORDER | WS_VISIBLE | WS_CHILD | ES_AUTOVSCROLL, 50, 580, 100, 25, hWnd, (HMENU)IDX_EDITBOX_03, hInst, NULL);
        _list1hWnd = CreateWindow(L"ListBox", NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | LBS_NOTIFY, 20, 180, 100, 200, hWnd, (HMENU)IDX_LISTBOX_01, hInst, NULL);
        _combo1hWnd = CreateWindow(L"ComboBox", NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | CBS_DROPDOWN, 20, 450, 100, 200, hWnd, (HMENU)IDX_COMBOBOX_01, hInst, NULL);
        _scroll1hWnd = CreateWindow(L"ScrollBar", NULL, WS_VISIBLE | WS_CHILD | SBS_VERT, 20, 580, 20, 200, hWnd, (HMENU)IDX_SCROLLBAR_01, hInst, NULL);
        
        SetScrollRange(_scroll1hWnd, SB_CTL, 0, 255, true);
        SetScrollPos(_scroll1hWnd, SB_CTL, 100, true);

        for (int i = 0; i < MAX_ITEM_COUNT; i++)
        {
            SendMessage(_list1hWnd, LB_ADDSTRING, 0, (LPARAM)_szItems[i]);
            SendMessage(_combo1hWnd, CB_ADDSTRING, 0, (LPARAM)_szItems[i]);
        }

        break;
    case WM_VSCROLL:
    {
        int barValue = GetScrollPos(_scroll1hWnd, SB_CTL);
        switch (LOWORD(wParam))
        {
        case SB_LINELEFT:
            barValue = max(0, barValue - 1);
            break;
        case SB_LINERIGHT:
            barValue = min(255, barValue + 1);
            break;
        case SB_PAGELEFT:
            barValue = max(0, barValue - 20);
            break;
        case SB_PAGERIGHT:
            barValue = min(255, barValue + 20);
            break;
        case SB_THUMBTRACK:
            barValue = HIWORD(wParam);
            break;
        default:
            break;
        }

        SetScrollPos(_scroll1hWnd, SB_CTL, barValue, true);
        wsprintf(_szBuffer, L"%d", barValue);
        SetWindowText(_edit3hWnd, _szBuffer);
    }
        break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 메뉴 선택을 구문 분석합니다:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_MODAL_DLG:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc);
                break;
            case IDM_MODALESS_DLG:
            {
                HWND hDlg = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG2), hWnd, DlgProc);
                if(hDlg) ShowWindow(hDlg, SW_SHOW);
            }
                break;
            case IDM_OPEN:
            {
                TCHAR dir[MAX_PATH] = { 0, };
                _tgetcwd(dir, MAX_PATH);
                OPENFILENAME OFN;
                memset(&OFN, 0, sizeof(OPENFILENAME));
                OFN.lStructSize = sizeof(OPENFILENAME);
                OFN.hwndOwner = hWnd;
                OFN.lpstrFilter = L"BMP 파일\0*.bmp;*.BMP\0모든 파일\0*.*\0";
                OFN.lpstrFile = _szFullPath;
                OFN.nMaxFile = 100;
                OFN.lpstrInitialDir = dir;
                if (GetOpenFileName(&OFN) != false)
                {
                    MessageBox(hWnd, _szFullPath, L"파일 경로 확인", MB_OK);
                    HorPieceCnt = 1;
                    VerPieceCnt = 1;
                    InvalidateRect(hWnd, NULL, true);
                }
            }
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            case ID_Hor2:
                HorPieceCnt = 2;
                InvalidateRect(hWnd, NULL, true);
                break;
            case ID_Hor3:
                HorPieceCnt = 3;
                InvalidateRect(hWnd, NULL, true);
                break;
            case ID_Hor4:
                HorPieceCnt = 4;
                InvalidateRect(hWnd, NULL, true);
                break;
            case ID_Ver2:
                VerPieceCnt = 2;
                InvalidateRect(hWnd, NULL, true);
                break;
            case ID_Ver3:
                VerPieceCnt = 3;
                InvalidateRect(hWnd, NULL, true);
                break;
            case ID_Ver4:
                VerPieceCnt = 4;
                InvalidateRect(hWnd, NULL, true);
                break;
            case IDX_BUTTON_01:
                //가로조각
                //MessageBox(hWnd, L"Click Me Button을 클릭 하셨습니다.", L"알림", MB_OK);
                if (isTransParentMode) alpha = min(alpha + 20, 255);
                else HorPieceCnt = HorPieceCnt % 5 + 1;
                InvalidateRect(hWnd, NULL, true);
                break;
            case IDX_BUTTON_02:
                //세로조각
                if (isTransParentMode) alpha = max(alpha - 20, 0);
                else VerPieceCnt = VerPieceCnt % 5 + 1;
                InvalidateRect(hWnd, NULL, true);
                break;
            case IDX_BUTTON_03:
                //삭제
            {
                int num = SendMessage(_list1hWnd, LB_GETCURSEL, 0, 0);
                num = num < 0 ? 0 : num;
                SendMessage(_list1hWnd, LB_DELETESTRING, num, 0);
            }
                break;
            case IDX_BUTTON_04:
                //추가
                //텍스트 Get
                GetWindowText(_edit2hWnd, _szBuffer, MAX_PATH);
                SendMessage(_list1hWnd, LB_ADDSTRING, 0, (LPARAM)_szBuffer);
                break;
            case IDX_CHECKBOX_01:
                if (SendMessage(_ckb1hWnd, BM_GETCHECK, 0, 0) == BST_CHECKED)
                {
                    SendMessage(_ckb1hWnd, BM_SETCHECK, BST_UNCHECKED, 0);
                    isTransParentMode = false;
                    SetWindowText(_btn1hWnd, button1Txt1);
                    SetWindowText(_btn2hWnd, button2Txt1);
                }
                else {
                    SendMessage(_ckb1hWnd, BM_SETCHECK, BST_CHECKED, 0);
                    isTransParentMode = true;
                    SetWindowText(_btn1hWnd, button1Txt2);
                    SetWindowText(_btn2hWnd, button2Txt2);
                }
            case IDX_EDITBOX_01:
                switch (HIWORD(wParam))
                {
                    //에디트 박스 텍스트 변경
                case EN_CHANGE:
                    GetWindowText(_edit1hWnd, _szBuffer, MAX_PATH);
                    SetWindowText(hWnd, _szBuffer);
                    break;
                default:
                    break;
                }
                break;

            case IDX_EDITBOX_02:
                switch (HIWORD(wParam))
                {
                    //에디트 박스 텍스트 변경
                case EN_SETFOCUS:
                    SetWindowText(_edit2hWnd, L"");
                    break;
                default:
                    break;
                }
                break;
            case IDX_LISTBOX_01:
                switch (HIWORD(wParam))
                {
                case LBN_SELCHANGE:
                {
                    int num = SendMessage(_list1hWnd, LB_GETCURSEL, 0, 0);
                    SendMessage(_list1hWnd, LB_GETTEXT, num, (LPARAM)_szBuffer);
                    SetWindowText(_edit1hWnd, _szBuffer);
                }
                    break;
                default:
                    break;
                }
                break;
            case IDX_COMBOBOX_01:
                switch (HIWORD(wParam))
                {
                case CBN_SELCHANGE:
                {
                    int num = SendMessage(_combo1hWnd, CB_GETCURSEL, 0, 0);
                    SendMessage(_combo1hWnd, CB_GETLBTEXT, num, (LPARAM)_szBuffer);
                    SetWindowText(_edit1hWnd, _szBuffer);
                }
                    break;
                case CBN_EDITCHANGE:
                    GetWindowText(_combo1hWnd, _szBuffer, MAX_PATH);
                    SetWindowText(hWnd, _szBuffer);
                    break;
                default:
                    break;
                }
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다...
            #pragma region DrawIamge

            //BITMAP bit;

            ////Get Memory DC
            //HDC mDC = CreateCompatibleDC(hdc);
            ////HBITMAP hBit = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP1)); //리소스에서 read
            //HBITMAP hBit = (HBITMAP)LoadImage(NULL , L"Data\\Sample.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE); //파일 경로에서 read
            //HBITMAP oldHBit = (HBITMAP)SelectObject(mDC, hBit);
            //
            //GetObject(hBit, sizeof(BITMAP), &bit); //bitmap에 대한 정보 Get

            //int onePieceWidth = bit.bmWidth / HorPieceCnt;
            //int onePieceHeight = bit.bmHeight/ VerPieceCnt;
            ////for (int i = 0; i < HorPieceCnt; i++)
            ////{
            ////    for (int j = 0; j < VerPieceCnt; j++)
            ////    {
            ////        BitBlt(hdc, 100 + (onePieceWidth + horGap) * i , 100 + (onePieceHeight + verGap) * j, onePieceWidth, onePieceHeight, mDC, onePieceWidth * i, onePieceHeight * j, SRCCOPY); // 메모리DC에서 그래픽DC로 카피
            ////    }
            ////}
            ////BitBlt(hdc, 100, 100, bit.bmWidth, bit.bmHeight, mDC, 0, 0, SRCCOPY); // 메모리DC에서 그래픽DC로 카피
            ////BitBlt(hdc, 600, 100, 100, 100, mDC, 100, 100, SRCCOPY); // 메모리DC에서 그래픽DC로 카피

            //for (int i = 0; i < HorPieceCnt; i++)
            //{
            //    for (int j = 0; j < VerPieceCnt; j++)
            //    {
            //        StretchBlt(hdc, 100 + (onePieceWidth + horGap) * i, 100 + (onePieceHeight + verGap) * j, onePieceWidth, onePieceHeight, mDC, onePieceWidth * i, onePieceHeight * j, onePieceWidth, onePieceHeight, SRCCOPY);
            //    }
            //}

            ////StretchBlt(hdc, 100, 50, 300, 300, mDC, 0, 0, bit.bmWidth, bit.bmHeight, SRCCOPY);
            //SelectObject(mDC, oldHBit);

            //DeleteDC(mDC);
            //BasicImageDraw::DrawTransparentsBMP(hInst, hdc, 100, 50, 500, 400, L"Data/BlackSample.bmp", RGB(0, 0, 0));

            BITMAP bit;
            HBITMAP hBit = (HBITMAP)LoadImage(NULL, _szFullPath, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject(hBit, sizeof(bit), &bit);
            int width = bit.bmWidth;
            int height = bit.bmHeight;

            int onepieceWidth = width / HorPieceCnt;
            int onepieceHeight = height / VerPieceCnt;

            if (isTransParentMode)
            {
                for (int i = 0; i < HorPieceCnt; i++)
                {
                    for (int j = 0; j < VerPieceCnt; j++)
                    {
                        _pBasicDraw->DrawAlphaBlendBMPPiece(hdc, 300 + i * (horGap + onepieceWidth), 50 + j * (verGap + onepieceHeight), onepieceWidth, onepieceHeight, onepieceWidth * i, onepieceHeight * j, onepieceWidth, onepieceHeight, _szFullPath, alpha);
                    }
                }

            }
            else {

                for (int i = 0; i < HorPieceCnt; i++)
                {
                    for (int j = 0; j < VerPieceCnt; j++)
                    {
                        _pBasicDraw -> DrawStretchBMPPiece(hdc, 300 + i * (horGap + onepieceWidth) , 50 + j * (verGap + onepieceHeight), onepieceWidth, onepieceHeight, onepieceWidth * i, onepieceHeight * j, onepieceWidth, onepieceHeight, _szFullPath);
                    }
                }
            }

            //_pBasicDraw -> DrawBitBMP(hdc, 300, 50, 200, 400, L"Data/BlackSample.bmp");
            //_pBasicDraw -> DrawStretchBMP(hdc, 300, 50, 200, 400, L"Data/BlackSample.bmp");
            //_pBasicDraw -> DrawTransparentsBMP(hdc, 550, 50, 200, 400, L"Data/BlackSample.bmp", RGB(20, 1, 20));
            //_pBasicDraw -> DrawAlphaBlendBMP(hdc, 800, 50, 200, 400, L"Data/BlackSample.bmp", 100);

#pragma endregion


            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        delete _pBasicDraw;
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

INT_PTR CALLBACK DlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        switch (LOWORD(wParam))
        {
        case IDOK:
        case IDCANCEL:
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
            break;
        case IDC_BUTTON1:
        {
            HWND btn = GetDlgItem(hDlg, IDC_BUTTON1);
        }
            break;
        default:
            break;
        }
        /*if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }*/
        break;
    }
    return (INT_PTR)FALSE;
}
