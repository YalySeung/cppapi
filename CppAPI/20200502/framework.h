﻿// header.h: 표준 시스템 포함 파일
// 또는 프로젝트 특정 포함 파일이 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
#include <iostream>

#include <windows.h>
#include <commdlg.h>
// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#define MAX_ITEM_COUNT 5

// Control ID
#define IDX_BUTTON_01 1001
#define IDX_BUTTON_02 1002
#define IDX_BUTTON_03 1003
#define IDX_BUTTON_04 1004

#define IDX_CHECKBOX_01 1101

#define IDX_EDITBOX_01 1201
#define IDX_EDITBOX_02 1202
#define IDX_EDITBOX_03 1203

#define IDX_LISTBOX_01 1301

#define IDX_COMBOBOX_01 1401

#define IDX_SCROLLBAR_01 1501