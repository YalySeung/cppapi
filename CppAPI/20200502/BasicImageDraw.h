#pragma once

class BasicImageDraw
{
	HINSTANCE _ownerInst;
	HWND _ownerhWnd;
	HDC _mdc;

public:
	BasicImageDraw(HINSTANCE hInst, HWND hWnd, HDC hdc);
	~BasicImageDraw();
	void DrawBitBMP(HDC hdc, int posX, int posY, int viewW, int viewH, const TCHAR* imgName);
	void DrawStretchBMP(HDC hdc, int posX, int posY, int viewW, int viewH, const TCHAR* imgName);
	void DrawTransparentsBMP(HDC hdc, int posX, int posY, int viewW, int viewH, const TCHAR* imgName, COLORREF transColor);
	void DrawAlphaBlendBMP(HDC hdc, int posX, int posY, int viewW, int viewH, const TCHAR* imgName, int alphaValue);
	void DrawStretchBMPPiece(HDC hdc, int posX, int posY, int viewW, int viewH, int bmpX, int bmpY, int bmpHeight, int bmpWidth, const TCHAR* imgName);
	void DrawAlphaBlendBMPPiece(HDC hdc, int posX, int posY, int viewW, int viewH, int bmpX, int bmpY, int bmpWidth, int bmpHeight, const TCHAR* imgName, int alphaValue);
};

