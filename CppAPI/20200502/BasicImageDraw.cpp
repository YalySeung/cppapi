#pragma once
#include "framework.h"
#include "BasicImageDraw.h"

BasicImageDraw::BasicImageDraw(HINSTANCE hInst, HWND hWnd, HDC hdc)
{
	_ownerInst = hInst;
	_ownerhWnd = hWnd;
	_mdc = CreateCompatibleDC(hdc);
}

BasicImageDraw::~BasicImageDraw()
{
	DeleteDC(_mdc);
}

void BasicImageDraw::DrawBitBMP(HDC hdc, int posX, int posY, int viewW, int viewH, const TCHAR* imgName)
{
	HBITMAP newBitmap, oldBitmap;
	BITMAP bit;

	newBitmap = (HBITMAP)LoadImage(_ownerInst, imgName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	oldBitmap = (HBITMAP)SelectObject(_mdc, newBitmap);
	GetObject(newBitmap, sizeof(BITMAP), &bit);
	BitBlt(hdc, posX, posY, viewW, viewH, _mdc, 0, 0, SRCCOPY);

	SelectObject(_mdc, oldBitmap);
	DeleteObject(newBitmap);
}

void BasicImageDraw::DrawStretchBMP(HDC hdc, int posX, int posY, int viewW, int viewH, const TCHAR* imgName)
{
	HBITMAP newBitmap, oldBitmap;
	BITMAP bit;

	newBitmap = (HBITMAP)LoadImage(_ownerInst, imgName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	oldBitmap = (HBITMAP)SelectObject(_mdc, newBitmap);
	GetObject(newBitmap, sizeof(BITMAP), &bit);
	StretchBlt(hdc, posX, posY, viewW, viewH, _mdc, 0, 0, bit.bmWidth, bit.bmHeight, SRCCOPY);

	SelectObject(_mdc, oldBitmap);
	DeleteObject(newBitmap);
}


void BasicImageDraw::DrawTransparentsBMP(HDC hdc, int posX, int posY, int viewW, int viewH, const TCHAR* imgName, COLORREF transColor)
{
	HBITMAP newBitmap, oldBitmap;
	BITMAP bit;

	newBitmap = (HBITMAP)LoadImage(_ownerInst, imgName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	oldBitmap = (HBITMAP)SelectObject(_mdc, newBitmap);
	GetObject(newBitmap, sizeof(BITMAP), &bit);
	TransparentBlt(hdc, posX, posY, viewW, viewH, _mdc, 0, 0, bit.bmWidth, bit.bmHeight, transColor);

	SelectObject(_mdc, oldBitmap);
	DeleteObject(newBitmap);
}

void BasicImageDraw::DrawAlphaBlendBMP(HDC hdc, int posX, int posY, int viewW, int viewH, const TCHAR* imgName, int alphaValue)
{
	HBITMAP newBitmap, oldBitmap;
	BITMAP bit;
	BLENDFUNCTION bf = {0,};

	ZeroMemory(&bf, sizeof(BLENDFUNCTION));
	bf.AlphaFormat = AC_SRC_OVER;
	bf.SourceConstantAlpha = alphaValue;
	newBitmap = (HBITMAP)LoadImage(_ownerInst, imgName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	oldBitmap = (HBITMAP)SelectObject(_mdc, newBitmap);
	GetObject(newBitmap, sizeof(BITMAP), &bit);
	AlphaBlend(hdc, posX, posY, viewW, viewH, _mdc, 0, 0, bit.bmWidth, bit.bmHeight, bf);

	SelectObject(_mdc, oldBitmap);
	DeleteObject(newBitmap);
}

void BasicImageDraw::DrawAlphaBlendBMPPiece(HDC hdc, int posX, int posY, int viewW, int viewH, int bmpX, int bmpY, int bmpWidth, int bmpHeight, const TCHAR* imgName, int alphaValue)
{
	HBITMAP newBitmap, oldBitmap;
	BITMAP bit;
	BLENDFUNCTION bf = { 0, };

	ZeroMemory(&bf, sizeof(BLENDFUNCTION));
	bf.AlphaFormat = AC_SRC_OVER;
	bf.SourceConstantAlpha = alphaValue;
	newBitmap = (HBITMAP)LoadImage(_ownerInst, imgName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	oldBitmap = (HBITMAP)SelectObject(_mdc, newBitmap);
	GetObject(newBitmap, sizeof(BITMAP), &bit);
	AlphaBlend(hdc, posX, posY, viewW, viewH, _mdc, bmpX, bmpY, bmpWidth, bmpHeight, bf);

	SelectObject(_mdc, oldBitmap);
	DeleteObject(newBitmap);
}

void BasicImageDraw::DrawStretchBMPPiece(HDC hdc, int posX, int posY, int viewW, int viewH, int bmpX, int bmpY, int bmpWidth, int bmpHeight, const TCHAR* imgName)
{
	HBITMAP newBitmap, oldBitmap;
	BITMAP bit;

	newBitmap = (HBITMAP)LoadImage(_ownerInst, imgName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	oldBitmap = (HBITMAP)SelectObject(_mdc, newBitmap);
	GetObject(newBitmap, sizeof(BITMAP), &bit);
	StretchBlt(hdc, posX, posY, viewW, viewH, _mdc, bmpX, bmpY, bmpWidth, bmpHeight, SRCCOPY);

	SelectObject(_mdc, oldBitmap);
	DeleteObject(newBitmap);
}


