﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// 20200412.rc에서 사용되고 있습니다.
//
#define IDC_MYICON                      2
#define IDD_MY20200412_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MY20200412                  107
#define IDI_SMALL                       108
#define IDC_MY20200412                  109
#define IDR_MAINFRAME                   128
#define ID_32771                        32771
#define ID_32772                        32772
#define IDM_MBOK                        32773
#define IDM_MB_YESNO                    32774
#define IDM_MBYESNO                     32775
#define IDM_LINE						32776
#define IDM_RECTANGLE                   32777
#define IDM_ELLAPSE                     32778
#define IDM_PEN							32779
#define IDM_BRUSH						32780
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
