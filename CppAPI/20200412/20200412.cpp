﻿// 20200412.cpp : 애플리케이션에 대한 진입점을 정의합니다.
//

#include "framework.h"
#include "20200412.h"
#include <ctime>
#include "Enums.h"

#define MAX_LOADSTRING 100

//전역 필수 변수.
HINSTANCE _hInst; //프로그램 식별자
HWND _hWnd;
TCHAR _nameClass[MAX_LOADSTRING] = { 0, };
TCHAR _nameTitle[MAX_LOADSTRING] = { 0, };
TCHAR _szString[MAX_LOADSTRING] = { 0, };

//메세지 프로시져.
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int MakeRandomTwoPoint(int area);
DrawType drawType;
POINT leftTop;
POINT rightBottom;
int x = 100, y = 300;
RECT _posLine;
bool isMouseDown = false;


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	srand((unsigned int)time(NULL));

	WNDCLASS wndClass;

	//.rc 파일의 StringTable String 값을 가져온다.
	LoadString(hInstance, IDS_APP_TITLE, _nameTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MY20200412, _nameClass, MAX_LOADSTRING);
	_hInst = hInstance;

	//윈도우 클래스의 레지스트 등록
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(IDI_MY20200412));
	wndClass.hInstance = hInstance;
	wndClass.lpfnWndProc = WndProc;
	wndClass.lpszClassName = _nameClass;
	wndClass.lpszMenuName = MAKEINTRESOURCE(IDC_MY20200412);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&wndClass);

	//윈도우 만들기
	_hWnd = CreateWindow(_nameClass, _nameTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, _hInst, NULL);

	if (!_hWnd)
	{
		return 0;
	}

	ShowWindow(_hWnd, nCmdShow);

	//HDC hdc = GetDC(_hWnd);
	////화면에  글씨 출력.(unicode)
	//const TCHAR* str = L"동해물과 백두산이..."; //L이 붙으면 Wchar가 된다(unicode)
	////TextOut(hdc, 100, 100, str, 12);
	////TextOut(hdc, 100, 100, str, sizeof(str));
	//TextOut(hdc, 100, 100, str, lstrlen(str));
	////SetTextAlign(hdc, TA_CENTER); //텍스트의 중간지점이 설정좌표
	//SetTextAlign(hdc, TA_CENTER); //텍스트의 중간지점이 설정좌표
	//TextOut(hdc, 100, 200, str, lstrlen(str));

	//ReleaseDC(_hWnd, hdc);

	drawType = TLine;
	leftTop = POINT();
	leftTop.x = 0;
	leftTop.y = 0;
	rightBottom = POINT();
	rightBottom.x = 0;
	rightBottom.y = 0;

	//메세지 루프
	MSG uMsg;
	while (GetMessage(&uMsg, NULL, 0, 0))
	{
		TranslateMessage(&uMsg);
		DispatchMessage(&uMsg);
	}

	return (int)uMsg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam); //바이트 절반으로 분리
		// 메뉴 선택을 구문 분석합니다:
		switch (wmId)
		{
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case IDM_ABOUT:
			MessageBox(NULL, L"메시지 박스가 열렸습니다.", L"알림", MB_OK); //소속 윈도우 X
			break;		
		case IDM_MBOK:
			MessageBox(hWnd, L"메시지 박스가 열렸습니다.", L"알림", MB_OK); //소속 윈도우 X
			break;		
		case IDM_MBYESNO:
			MessageBox(hWnd, L"메시지 박스가 열렸습니다.", L"알림", MB_YESNO); //소속 윈도우 X
			break;
		case IDM_LINE:
		{
			RECT rect = { 0,0,0,0 };
			GetWindowRect(_hWnd, &rect);
			drawType = TLine;
			MakeRandomTwoPoint(rect.bottom);
			InvalidateRect(_hWnd, NULL, true);
		}
		break;
		case IDM_RECTANGLE:
		{
			RECT rect = { 0,0,0,0 };
			GetWindowRect(_hWnd, &rect);
			drawType = TRect;
			MakeRandomTwoPoint(rect.bottom);
			InvalidateRect(_hWnd, NULL, true);
		break;
		}
		case IDM_ELLAPSE:
		{
			RECT rect = { 0,0,0,0 };
			GetWindowRect(_hWnd, &rect);
			drawType = TCircle;
			MakeRandomTwoPoint(rect.bottom);
			InvalidateRect(_hWnd, NULL, true);
		}
		break;
		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{

		#pragma region Sample
			//PAINTSTRUCT ps;
			//HDC hdc = BeginPaint(_hWnd, &ps);

			//POINT pt;

			//const TCHAR* str = L"동해물과 백두산이 마르고 닳도록 하느님이 보우하사 우리 나라만세";

			////글쓰기
			////TextOut(hdc, 100, 100, str, lstrlen(str));

			//직사각형 그리기
			//RECT rect = { 100, 100, 300, 300 }; // => 초기화 할때만 가능
			//RECT rect2 = { 100, 500, 300, 800 }; // => 초기화 할때만 가능
			//Rectangle(hdc, rect.left-2, rect.top-2, rect.right, rect.bottom); //직사각형
			//DrawText(hdc, str, -1, &rect, DT_TOP | DT_WORDBREAK);
			//DrawText(hdc, str, -1, &rect2, DT_VCENTER| DT_WORDBREAK);

			//점찍기
			//SetPixel(hdc, 102, 100, RGB(255, 0, 0)); 

			//라인그리기
			//int offset = 200;
			//MoveToEx(hdc, offset + 200, offset + 0, &pt); //시작점 설정
			//LineTo(hdc, offset + 75, offset + 400);
			//LineTo(hdc, offset + 400, offset + 150);
			//LineTo(hdc, offset + 0, offset + 150);
			//LineTo(hdc, offset + 325, offset + 400);
			//LineTo(hdc, offset + 200, offset + 0);

			//Ellipse(hdc, 500, 100, 800, 400); //타원

			////메시지 박스
			////MessageBox(_hWnd, L"메시지 박스가 열렸습니다.", L"알림", MB_OK);
			////MessageBox(NULL, L"메시지 박스가 열렸습니다.", L"알림", MB_OK); //소속 윈도우 X
			//EndPaint(_hWnd, &ps);
		#pragma endregion


		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(_hWnd, &ps);

		POINT pt;

		//도형 그리기
		switch (drawType)
		{
		case TLine:
			MoveToEx(hdc, leftTop.x, leftTop.y, &pt); //시작점 설정
			LineTo(hdc, rightBottom.x, rightBottom.y);
			break;
		case TRect:
			Rectangle(hdc, leftTop.x, leftTop.y, rightBottom.x, rightBottom.y); //직사각형
			break;
		case TCircle:
			Ellipse(hdc, leftTop.x, leftTop.y, rightBottom.x, rightBottom.y); //타원
			break;
		default:
			break;
		}
		MoveToEx(hdc, _posLine.left, _posLine.top, &pt); //시작점 설정
		LineTo(hdc, _posLine.right, _posLine.bottom);
		TextOut(hdc, x, y, _szString, lstrlen(_szString));

		EndPaint(_hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		int len = lstrlen(_szString);
		_szString[len] = (TCHAR)wParam;
		_szString[len + 1] = '\0';
		InvalidateRect(hWnd, NULL, true);
	}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_LEFT:
		case 'A':
			x -= 8;
			break;
		case VK_RIGHT:
		case 'D':
			x += 8;
			break;
		case VK_UP:
		case 'W':
			y -= 8;
			break;
		case VK_DOWN:
		case 'S':
			y += 8;
			break;
		default:
			break;
		}
		InvalidateRect(hWnd, NULL, true); //다시 그리기
		break;
	/*case WM_LBUTTONDOWN:
		_posLine.left = LOWORD(lParam);
		_posLine.top = HIWORD(lParam);
		InvalidateRect(hWnd, NULL, true);
		break;
	case WM_RBUTTONDOWN:
		_posLine.right = LOWORD(lParam);
		_posLine.bottom = HIWORD(lParam);
		InvalidateRect(hWnd, NULL, true);
		break;*/
	case WM_LBUTTONDOWN:
		//isMouseDown = true;
		_posLine.left = LOWORD(lParam);
		_posLine.top = HIWORD(lParam);
		break;
	case WM_MOUSEMOVE:
		if (wParam & MK_LBUTTON)
		{
			_posLine.right = LOWORD(lParam);
			_posLine.bottom = HIWORD(lParam);
			InvalidateRect(hWnd, NULL, true);
		}
		break;
	//case WM_LBUTTONUP:
	//	isMouseDown = false;
	//	InvalidateRect(hWnd, NULL, true);
	//	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

// 랜덤 범위 숫자 리턴
int MakeRandomTwoPoint(int area) {
	int temp = 0;

	leftTop.x = rand() % area;
	leftTop.y = rand() % area;
	temp = rand() % area;
	if (temp < leftTop.x)
	{
		rightBottom.x = leftTop.x;
		leftTop.x = temp;
	}
	temp = rand() % area;
	if (temp < leftTop.y)
	{
		rightBottom.y = leftTop.y;
		leftTop.y = temp;
	}
	return 0;
}