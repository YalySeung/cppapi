﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// 20200419.rc에서 사용되고 있습니다.
//
#define IDC_MYICON                      2
#define IDD_MY20200419_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MY20200419                  107
#define IDI_SMALL                       108
#define IDC_MY20200419                  109
#define IDR_MAINFRAME                   128
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define IDM_LINE                        32774
#define IDM_RECT                        32775
#define IDM_CIRCLE                      32776
#define IDM_PEN                         32777
#define IDM_BRUSH                       32778
#define IDM_PENST1                      32779
#define IDM_PENST2                      32780
#define IDM_PENST3                      32781
#define IDM_PENST4                      32782
#define IDM_BRUSHST1                    32783
#define IDM_BRUSHST2                    32784
#define IDM_BRUSHST3                    32785
#define IDM_BRUSHST4                    32786
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
