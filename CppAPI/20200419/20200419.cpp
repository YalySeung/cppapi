﻿// 20200419.cpp : 애플리케이션에 대한 진입점을 정의합니다.
//

#include "framework.h"
#include <vector>
#include "20200419.h"
#include "Figure.h"

using namespace std;
#define MAX_LOADSTRING 100

// 전역 변수:
HINSTANCE hInst;                                // 현재 인스턴스입니다.
WCHAR szTitle[MAX_LOADSTRING];                  // 제목 표시줄 텍스트입니다.
WCHAR szWindowClass[MAX_LOADSTRING];            // 기본 창 클래스 이름입니다.
WCHAR szString[MAX_LOADSTRING];
// 이 코드 모듈에 포함된 함수의 선언을 전달합니다:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

//선, 면, 원 그리기
DrawType drawType = DTLine;
RECT lineDrawRect;
RECT rectDrawRect;
RECT circleDrawRect;

bool isUsePreView = true;

int timeCount = 0;
POINT _txtPosition = { 100, 20 };
int radius = 1;
POINT circleCenterPoint = { 100, 200 };
bool CanMoveCircle = false;

Figure figureTemp;
vector<Figure> vf;
vector<HPEN> vp;
vector<HBRUSH> vb;

HPEN currentPen;
HBRUSH currentBrush;
DrawType currentDrawType;

void CALLBACK TimerProc(HWND hWnd, UINT message, UINT_PTR uId, DWORD lParam);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: 여기에 코드를 입력합니다.

	// 전역 문자열을 초기화합니다.
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_MY20200419, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// 애플리케이션 초기화를 수행합니다:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY20200419));

	MSG msg;

	// 기본 메시지 루프입니다:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  함수: MyRegisterClass()
//
//  용도: 창 클래스를 등록합니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY20200419));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MY20200419);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   용도: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   주석:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

void CALLBACK TimerProc(HWND hWnd, UINT message, UINT_PTR uId, DWORD lParam)
{
	if (uId == _TIMER_01)
	{
		int min, sec;
		timeCount++;
		min = timeCount / 60;
		sec = timeCount % 60;
		if (min < 10)
		{
			if (sec < 10)
			{
				wsprintf(szString, L"0%d : 0%d", min, sec);
			}
			else {
				wsprintf(szString, L"0%d : %d", min, sec);
			}
		}
		else {
			if (sec < 10)
			{
				wsprintf(szString, L"%d : 0%d", min, sec);
			}
			else {
				wsprintf(szString, L"%d : %d", min, sec);
			}
		}
		InvalidateRect(hWnd, NULL, true);
	}
	else if (uId == _TIMER_02)
	{
		_txtPosition.x += 1;
		InvalidateRect(hWnd, NULL, true);
	}
	else if (uId == _TIMER_03) {
		int nextX = circleCenterPoint.x + 1;
		if (nextX % 600 == 0) nextX = 100;
		circleCenterPoint.x = nextX;
		InvalidateRect(hWnd, NULL, true);
	}
	else if (uId == _TIMER_04) {
		radius += 1;
		InvalidateRect(hWnd, NULL, true);
	}

}


//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  용도: 주 창의 메시지를 처리합니다.
//
//  WM_COMMAND  - 애플리케이션 메뉴를 처리합니다.
//  WM_PAINT    - 주 창을 그립니다.
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		SetTimer(hWnd, _TIMER_01, 1000, TimerProc);
		SetTimer(hWnd, _TIMER_02, 300, TimerProc);

		//벡터, 브러시 초기화
		vb.push_back((HBRUSH)CreateHatchBrush(NULL, RGB(255, 0, 0)));
		vb.push_back((HBRUSH)CreateHatchBrush(HS_CROSS, RGB(0, 255, 0)));
		vb.push_back((HBRUSH)CreateHatchBrush(HS_DIAGCROSS, RGB(0, 0, 255)));
		vb.push_back((HBRUSH)CreateHatchBrush(HS_HORIZONTAL, RGB(0, 0, 0)));
		vp.push_back((HPEN)CreatePen(PS_DASH, 1, RGB(255, 0, 0)));
		vp.push_back((HPEN)CreatePen(PS_DOT, 2, RGB(0, 255, 0)));
		vp.push_back((HPEN)CreatePen(PS_DASHDOT, 2, RGB(0, 0, 255)));
		vp.push_back((HPEN)CreatePen(PS_DASHDOTDOT, 1, RGB(0, 0, 0)));

		currentPen = vp.at(0);
		currentBrush = vb.at(0);
		break;
		//case WM_TIMER:
		//{
		//    if (wParam == _TIMER_01)
		//    {
		//        int min, sec;
		//        timeCount++;
		//        min = timeCount / 60;
		//        sec = timeCount % 60;
		//        if (min < 10)
		//        {
		//            if (sec < 10)
		//            {
		//                wsprintf(szString, L"0%d : 0%d", min, sec);
		//            }
		//            else {
		//                wsprintf(szString, L"0%d : %d", min, sec);
		//            }
		//        }
		//        else {
		//            if (sec < 10)
		//            {
		//                wsprintf(szString, L"%d : 0%d", min, sec);
		//            }
		//            else {
		//                wsprintf(szString, L"%d : %d", min, sec);
		//            }
		//        }
		//    }
		//}
		//    break;  
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// 메뉴 선택을 구문 분석합니다:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case IDM_LINE:
			drawType = DTLine;
			currentDrawType = DTLine;
			break;
		case IDM_RECT:
			drawType = DTRect;
			currentDrawType = DTRect;
			break;
		case IDM_CIRCLE:
			drawType = DTCircle;
			currentDrawType = DTCircle;
			break;
		case IDM_PENST1:
			currentPen = vp.at(0);
			break;
		case IDM_PENST2:
			currentPen = vp.at(1);
			break;
		case IDM_PENST3:
			currentPen = vp.at(2);
			break;
		case IDM_PENST4:
			currentPen = vp.at(3);
			break;
		case IDM_BRUSHST1:
			currentBrush = vb.at(0);
			break;
		case IDM_BRUSHST2:
			currentBrush = vb.at(1);
			break;
		case IDM_BRUSHST3:
			currentBrush = vb.at(2);
			break;
		case IDM_BRUSHST4:
			currentBrush = vb.at(3);
			break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다...
		//POINT point;
		//MoveToEx(hdc, lineDrawRect.left, lineDrawRect.top, &point);
		//LineTo(hdc, lineDrawRect.right, lineDrawRect.bottom);
		//Rectangle(hdc, rectDrawRect.left, rectDrawRect.top, rectDrawRect.right, rectDrawRect.bottom);
		//Ellipse(hdc, circleDrawRect.left, circleDrawRect.top, circleDrawRect.right, circleDrawRect.bottom);
		//TextOut(hdc, _txtPosition.x, _txtPosition.y, szString, lstrlen(szString));

		//이동하는 원
		//Ellipse(hdc, circleCenterPoint.x - radius, circleCenterPoint.y + radius, circleCenterPoint.x + radius, circleCenterPoint.y - radius);

#pragma region brush, pen
		//HBRUSH oldBrush, newBrush;
		//HPEN oldPen, newPen;

		//newPen = CreatePen(PS_DOT, 2, RGB(80, 101, 255));
		//oldPen = (HPEN)SelectObject(hdc, newPen);
		//newBrush = CreateHatchBrush(BS_NULL, RGB(NULL, 101, 255));
		//oldBrush = (HBRUSH)SelectObject(hdc, newBrush);
		//Ellipse(hdc, 300, 100, 400, 200);
		//DeleteObject(newBrush);
		//DeleteObject(newPen);

		//newPen = CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
		//SelectObject(hdc, newPen);
		//newBrush = CreateSolidBrush(RGB(0, 0, 0));
		//SelectObject(hdc, newBrush);
		//Ellipse(hdc, 380, 100, 480, 200);
		//DeleteObject(newBrush);
		//DeleteObject(newPen);

		//newPen = CreatePen(PS_DASH, 2, RGB(255, 0, 0));
		//SelectObject(hdc, newPen);
		//newBrush = CreateHatchBrush(HS_BDIAGONAL, RGB(255, 0, 0));
		//SelectObject(hdc, newBrush);
		//Ellipse(hdc, 460, 100, 560, 200);
		//DeleteObject(newBrush);
		//DeleteObject(newPen);

		//newPen = CreatePen(PS_DASHDOT, 2, RGB(239, 242, 29));
		//SelectObject(hdc, newPen);
		//newBrush = CreateHatchBrush(HS_FDIAGONAL, RGB(239, 242, 29));
		//SelectObject(hdc, newBrush);
		//Ellipse(hdc, 340, 180, 440, 280);
		//DeleteObject(newBrush);
		//DeleteObject(newPen);

		//newPen = CreatePen(PS_DASHDOTDOT, 2, RGB(0, 255, 0));
		//SelectObject(hdc, newPen);
		//newBrush = CreateHatchBrush(HS_CROSS, RGB(0, 255, 0));
		//SelectObject(hdc, newBrush);
		//Ellipse(hdc, 420, 180, 520, 280);
		//SelectObject(hdc, oldPen);
		//SelectObject(hdc, oldBrush);

		//DeleteObject(newBrush);
		//DeleteObject(newPen);


		//newBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
		//oldBrush = (HBRUSH)SelectObject(hdc, newBrush);

		//Rectangle(hdc, 100, 400, 150, 550);
		//SelectObject(hdc, oldBrush);
		//Rectangle(hdc, 150, 400, 180, 550);
		//DeleteObject(newBrush);

		//newBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
		//oldBrush = (HBRUSH)SelectObject(hdc, newBrush);
		//newPen = (HPEN)GetStockObject(BLACK_PEN);
		//oldPen = (HPEN)SelectObject(hdc, newPen);
		//newPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
		//oldPen = (HPEN)SelectObject(hdc, newPen);
		//newBrush = (HBRUSH)CreateHatchBrush(HS_CROSS, RGB(255, 100, 0));
		//oldBrush = (HBRUSH)SelectObject(hdc, newBrush);

		//Ellipse(hdc, 180, 400, 250, 550);
		//SelectObject(hdc, oldPen);
		//SelectObject(hdc, oldBrush);
		//Ellipse(hdc, 250, 400, 300, 550);
		//DeleteObject(newPen);
		//DeleteObject(newBrush);
		//DeleteObject(newBrush);
		//DeleteObject(newPen);
#pragma endregion
		HFONT newFont;
		HFONT oldFont;
		LOGFONT lf;
		lf = { 50, 0, 0, 0, 0, 0, 0, 0, HANGUL_CHARSET, 0, 0, 0, 0, L"궁서" };
		//newFont = CreateFont(50, 0, 0, 0, 0, 0, 0, 0, HANGUL_CHARSET, 0, 0, 0, 0, L"궁서");
		newFont = CreateFontIndirect(&lf);
		oldFont = (HFONT)SelectObject(hdc, newFont);

		SetTextColor(hdc, RGB(0, 255, 0));
		SetBkColor(hdc, RGB(255, 0, 255));

		LPCWSTR stringTemp = L"안녕하세요";
		TextOut(hdc, 800, 200, stringTemp, lstrlen(stringTemp));

		SetTextColor(hdc, RGB(0, 0, 0));
		SetBkColor(hdc, RGB(255, 255, 0));
		SetBkMode(hdc, TRANSPARENT);
		TextOut(hdc, 800, 220, stringTemp, lstrlen(stringTemp));

		SelectObject(hdc, oldFont);
		DeleteObject(newFont);

		//그리기 정보
		for (int i = 0; i < vf.size(); i++)
		{
			vf[i].DrawFigure(&hdc);
		}


		EndPaint(hWnd, &ps);
	}
	break;
	case WM_LBUTTONDOWN:
		switch (drawType)
		{
		case DTLine:
			lineDrawRect.left = LOWORD(lParam);
			lineDrawRect.top = HIWORD(lParam);
			break;
		case DTRect:
			rectDrawRect.left = LOWORD(lParam);
			rectDrawRect.top = HIWORD(lParam);
			break;
		case DTCircle:
			circleDrawRect.left = LOWORD(lParam);
			circleDrawRect.top = HIWORD(lParam);
			break;
		default:
			break;
		}
		figureTemp = Figure();
		figureTemp.penInfo = currentPen;
		figureTemp.brushInfo = currentBrush;
		figureTemp.shapeInfo.left = LOWORD(lParam);
		figureTemp.shapeInfo.top = HIWORD(lParam);
		figureTemp.drawType = currentDrawType;
		vf.push_back(figureTemp.Clone());
	case WM_MOUSEMOVE:
		if (wParam & MK_LBUTTON)
		{
			if (!(wParam & MK_SHIFT) || !(wParam & MK_CONTROL))
			{
				switch (drawType)
				{
				case DTLine:
					lineDrawRect.right = LOWORD(lParam);
					lineDrawRect.bottom = HIWORD(lParam);
					break;
				case DTRect:
					rectDrawRect.right = LOWORD(lParam);
					rectDrawRect.bottom = HIWORD(lParam);
					break;
				case DTCircle:
					circleDrawRect.right = LOWORD(lParam);
					circleDrawRect.bottom = HIWORD(lParam);
					break;
				default:
					break;
				}
				figureTemp.shapeInfo.right = LOWORD(lParam);
				figureTemp.shapeInfo.bottom = HIWORD(lParam);
				if(!vf.empty()) vf.pop_back();
				vf.push_back(figureTemp.Clone());
				InvalidateRect(hWnd, NULL, true);
			}
		}
		break;
	case WM_LBUTTONUP:
		switch (drawType)
		{
		case DTLine:
			lineDrawRect.right = LOWORD(lParam);
			lineDrawRect.bottom = HIWORD(lParam);
			break;
		case DTRect:
			rectDrawRect.right = LOWORD(lParam);
			rectDrawRect.bottom = HIWORD(lParam);
			break;
		case DTCircle:
			circleDrawRect.right = LOWORD(lParam);
			circleDrawRect.bottom = HIWORD(lParam);
			break;
		default:
			break;
		}
		figureTemp.shapeInfo.right = LOWORD(lParam);
		figureTemp.shapeInfo.bottom = HIWORD(lParam);
		if (!vf.empty()) vf.pop_back();
		vf.push_back(figureTemp.Clone());
		InvalidateRect(hWnd, NULL, true);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			CanMoveCircle = !CanMoveCircle;
			if (CanMoveCircle == true)
			{
				SetTimer(hWnd, _TIMER_03, 1000 / 30, TimerProc);
				SetTimer(hWnd, _TIMER_04, 2000, TimerProc); //늘어남
			}
			else {
				KillTimer(hWnd, _TIMER_03);
				KillTimer(hWnd, _TIMER_04);
			}
			break;
		default:
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

