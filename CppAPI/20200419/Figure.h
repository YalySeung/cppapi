#pragma once
#include "framework.h"
#include "Enums.h"

class Figure
{
private:

public :
	Figure();
	DrawType drawType;
	RECT shapeInfo;
	HPEN penInfo;
	HBRUSH brushInfo;
	bool DrawFigure(HDC* _hdc);
	Figure Clone();
};

