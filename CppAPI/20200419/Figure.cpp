#include "Figure.h"

Figure::Figure()
{
	drawType = DTLine;
	shapeInfo = { 0,0,0,0 };
	penInfo = CreatePen(PS_NULL, 1, RGB(0, 0, 0));
	brushInfo = CreateHatchBrush(BS_NULL, RGB(0, 0, 0));
}

bool Figure::DrawFigure(HDC* _hdc)
{

	HPEN oldPen;
	HBRUSH oldBrush;
	oldBrush = (HBRUSH)SelectObject(*_hdc, brushInfo);
	oldPen = (HPEN)SelectObject(*_hdc, penInfo);

	switch (drawType)
	{
	case DTLine: {
		POINT point;
		MoveToEx(*_hdc, shapeInfo.left, shapeInfo.top, &point);
		LineTo(*_hdc, shapeInfo.right, shapeInfo.bottom);
	}
		break;
	case DTRect:
		Rectangle(*_hdc, shapeInfo.left, shapeInfo.top, shapeInfo.right, shapeInfo.bottom);
		break;
	case DTCircle:
		Ellipse(*_hdc, shapeInfo.left, shapeInfo.top, shapeInfo.right, shapeInfo.bottom);
		break;
	default:
		break;
	}
	SelectObject(*_hdc, oldBrush);
	SelectObject(*_hdc, oldPen);

	return false;
}

Figure Figure::Clone()
{	
	Figure retFigure;
	retFigure.drawType = this->drawType;
	retFigure.shapeInfo = this->shapeInfo;
	retFigure.penInfo = this->penInfo;
	retFigure.brushInfo = this->brushInfo;
	return retFigure;
}
