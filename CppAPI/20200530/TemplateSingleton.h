#pragma once


template <typename T>
class TemplateSingleton
{
protected:
	TemplateSingleton() {};
	virtual ~TemplateSingleton() {};

public:
	static T* GetInstance() {
		{
			if (m_pInstance == nullptr)
			{
				m_pInstance = new T;
			}
			return m_pInstance;
		}
	};
	static void DestoryInstance();

private:
	static T* m_pInstance;
};

template <typename T> T* TemplateSingleton<T>::m_pInstance = 0;


