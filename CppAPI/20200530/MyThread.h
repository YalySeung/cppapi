#pragma once

#include "headers.h"
class MyThread
{
protected:
	virtual void threadMain() {};

	static unsigned long __stdcall __threadMain(void* args) {
		MyThread* self = (MyThread*)args;
		self->threadMain();
		return 0;
	}
public :
	void run() {
		CreateThread(0, 0, __threadMain, (void*)this, 0, 0);
	}
};

