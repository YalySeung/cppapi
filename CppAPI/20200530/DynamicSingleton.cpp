#include "headers.h"
#include "DynamicSingleton.h"


DynamicSingleton* DynamicSingleton::instance;

DynamicSingleton::DynamicSingleton()
{
}

DynamicSingleton::DynamicSingleton(const DynamicSingleton& other)
{
}

DynamicSingleton::~DynamicSingleton()
{
}

void DynamicSingleton::Destroy()
{
	
}

DynamicSingleton* DynamicSingleton::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new DynamicSingleton();
		atexit(Destroy);
	}
	return instance;
}

void DynamicSingleton::PrintScreen()
{
	cout << "화면에 저장" << endl;
}


