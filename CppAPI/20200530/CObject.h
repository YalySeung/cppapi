#pragma once

class CObject :public TemplateSingleton<CObject>
{
public: 
	CObject();
	~CObject();
	void PrintScreen();
};

