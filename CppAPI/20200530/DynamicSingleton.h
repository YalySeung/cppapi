#pragma once

class DynamicSingleton
{
private :
	DynamicSingleton();
	DynamicSingleton(const DynamicSingleton& other);
	~DynamicSingleton();

	static DynamicSingleton* instance;
	static void Destroy();

public:
	static DynamicSingleton* GetInstance();
	void PrintScreen();

};

