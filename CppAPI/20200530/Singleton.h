#pragma once
class Singleton
{
private:
	Singleton() {};
	Singleton(const Singleton& other);

	static Singleton Instance;

public :
	static Singleton* GetInstance() { return &Instance; };
	~Singleton() {};
	void PrintScreen();
};

