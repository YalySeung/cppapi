#include "TemplateSingleton.h"

template<typename T>
void TemplateSingleton<T>::DestoryInstance()
{
	if (m_pInstance)
	{
		delete m_pInstance;
		m_pInstance = nullptr;
	}
}