#include "MyWindow.h"
#include "resource.h"


MyWindow* MyWindow::pMainWindow = 0;
ImageDraw _imageDraw;
HWND _btnStartAnimation;
int imageNum = 1;
bool _isAnimating = false;
int xPos = 100;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    return MyWindow::pMainWindow->MyWndProc(hWnd, message, wParam, lParam);
}

MyWindow::MyWindow()
{
    pMainWindow = this;
    memset(szTitle, 0, MAX_LOADSTRING);                  // 제목 표시줄 텍스트입니다.
    memset(szWindowClass, 0, MAX_LOADSTRING);                  // 기본 창 클래스 이름입니다.
}

BOOL MyWindow::InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.


    hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

    if (!hWnd)
    {
        return FALSE;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    return TRUE;

}

BOOL MyWindow::InitChildInstance(HINSTANCE hInstance, int nCmdShow, LPCWSTR className, HWND parentHwnd, LPCWSTR caption)
{
    hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

    hWnd = CreateWindow(className, caption, WS_CHILD | WS_VISIBLE | WS_BORDER | WS_CAPTION, 10, 10, 200, 500, parentHwnd, NULL, hInst, NULL);

    if (!hWnd)
    {
        return FALSE;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    return TRUE;

}


ATOM MyWindow::MyRegisterClass(HINSTANCE hInstance)
{
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY202005161, szWindowClass, MAX_LOADSTRING);
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY202005161));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MY202005161);
    wcex.lpszClassName = szWindowClass;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

ATOM MyWindow::MyRegisterClassCustom(HINSTANCE hInstance, LPCWSTR className)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY202005161));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MY202005161);
    wcex.lpszClassName = className;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

int MyWindow::MessageLoop()
{
    HACCEL hAccelTable = LoadAccelerators(hInst, MAKEINTRESOURCE(IDC_MY202005161));

    MSG msg;

    // 기본 메시지 루프입니다:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int)msg.wParam;
}
LRESULT MyWindow::MyWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:

        _btnStartAnimation = CreateWindow(L"Button", L"START", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 400, 100, 200, 50, hWnd, (HMENU)CTRL_BUTTON1, hInst, NULL);
        break;

    case WM_COMMAND:
    {
        int wmId = LOWORD(wParam);
        // 메뉴 선택을 구문 분석합니다:
        switch (wmId)
        {
        case IDM_ABOUT:
            break;
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
        case IDM_CHILDWND: {

            MyWindow childWindow;
            childWindow.MyRegisterClassCustom(hInst, L"Child");
            if (!childWindow.InitChildInstance(hInst, 10, L"Child", hWnd, L"자식"))
                return false;

            return childWindow.MessageLoop();
        }
                         break;
        case CTRL_BUTTON1 :
            if (_isAnimating)
            {
                KillTimer(hWnd, _TIMER_01);
                SetWindowText(_btnStartAnimation, L"START");
                _isAnimating = false;
            }
            else {
                SetTimer(hWnd, _TIMER_01, 50, NULL);
                SetWindowText(_btnStartAnimation, L"STOP");
                _isAnimating = true;
            }
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    break;
    case WM_LBUTTONDOWN:
        MessageBox(hWnd, L"안녕", L"알림", MB_OK);
        break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_RIGHT:
            xPos += 10;
            break;
        case VK_LEFT:
            xPos -= 10;
            break;
        }
        InvalidateRect(hWnd, NULL, false);
        break;
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        // TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다...
        //_imageDraw.DrawImageToFileName(hdc, 100, 100, L"C:/Workspace/CppApi/CppAPI/20200516_1/Data/Sample.png");
        //_imageDraw.DrawStretchImageToFileName(hdc, 100, 100, 400, 500, L"C:/Workspace/CppApi/CppAPI/20200516_1/Data/Sample.png");
        //_imageDraw.DrawStretchPartialImageToFileName(hdc, 100, 100, 200, 200, 150 * imageNum, 150, 145, 130, L"C:/Workspace/CppApi/CppAPI/20200516_1/Data/Bird.png");
        //_imageDraw.DrawBMPToFileName(hInst, hdc, xPos, 100, L"C:/Workspace/CppApi/CppAPI/20200516_1/Data/BlackSample.bmp");
        //_imageDraw.DrawBMPToFileName_DB(hInst, hWnd, hdc, xPos, 100, L"C:/Workspace/CppApi/CppAPI/20200516_1/Data/BlackSample.bmp");
        _imageDraw.DrawStretchPartialImageToFileName_DB(hWnd ,hdc, 100, 100, 200, 200, 150 * imageNum, 150, 145, 130, L"C:/Workspace/CppApi/CppAPI/20200516_1/Data/Bird.png");
        EndPaint(hWnd, &ps);
    }
    case WM_TIMER:
        if (wParam == _TIMER_01)
        {
            imageNum = imageNum % 5 + 1;
            InvalidateRect(hWnd, NULL, false);
        }
        break;
    break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
