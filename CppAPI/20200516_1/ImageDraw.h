#pragma once
class ImageDraw
{
	ULONG_PTR _gdiPlusToken;

public :
	ImageDraw();
	~ImageDraw();

	void DrawImageToFileName(HDC hdc, int posX, int posY, LPCWSTR fullName);
	void DrawStretchImageToFileName(HDC hdc, int posX, int posY, int w, int h, LPCWSTR fullName);
	void DrawStretchPartialImageToFileName(HDC hdc, int posX, int posY, int w, int h, int tposX, int tposY, int tw, int th, LPCWSTR fullName);
	void DrawStretchPartialImageToFileName_DB(HWND hWnd, HDC hdc, int posX, int posY, int w, int h, int tposX, int tposY, int tw, int th, LPCWSTR fullName);


	void DrawBMPToFileName(HINSTANCE hInst, HDC hdc, int posX, int posY, LPCWSTR fullName);
	void DrawBMPToFileName_DB(HINSTANCE hInst, HWND hWnd, HDC hdc, int posX, int posY, LPCWSTR fullName);
};

