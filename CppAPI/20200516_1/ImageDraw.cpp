#include "framework.h"
#include "resource.h"

#include "ImageDraw.h"



ImageDraw::ImageDraw()
{
	GdiplusStartupInput gdiInput;
	GdiplusStartup(&_gdiPlusToken, &gdiInput, NULL);
}

ImageDraw::~ImageDraw()
{
	GdiplusShutdown(_gdiPlusToken);
}

void ImageDraw::DrawImageToFileName(HDC hdc, int posX, int posY, LPCWSTR fullName)
{
	Graphics graphics(hdc); //mdc 생성
	Image image(fullName); //loadImage SelectObject
	graphics.DrawImage(&image, posX, posY); //메모리 복사
}

void ImageDraw::DrawStretchImageToFileName(HDC hdc, int posX, int posY, int w, int h, LPCWSTR fullName)
{
	Graphics graphics(hdc); //mdc 생성
	Image image(fullName); //loadImage SelectObject
	Rect rt;
	rt.X = posX;
	rt.Y = posY;
	if (w == 0 || h == 0)
	{
		rt.Width = image.GetWidth();
		rt.Height = image.GetWidth();
	}
	else {
		rt.Width = w;
		rt.Height = h;
	} 
	graphics.DrawImage(&image, rt); //메모리 복사
}

void ImageDraw::DrawStretchPartialImageToFileName(HDC hdc, int posX, int posY, int w, int h, int tposX, int tposY, int tw, int th, LPCWSTR fullName)
{
	Graphics graphics(hdc); //mdc 생성
	Image image(fullName); //loadImage SelectObject
	Rect rt;
	rt.X = posX;
	rt.Y = posY;
	if (w == 0 || h == 0)
	{
		rt.Width = image.GetWidth();
		rt.Height = image.GetWidth();
	}
	else {
		rt.Width = w;
		rt.Height = h;
	}
	graphics.DrawImage(&image, rt, tposX, tposY, tw, th, UnitPixel); //메모리 복사
}

void ImageDraw::DrawStretchPartialImageToFileName_DB(HWND hWnd, HDC hdc, int posX, int posY, int w, int h, int tposX, int tposY, int tw, int th, LPCWSTR fullName)
{
	RECT rtWnd;
	GetClientRect(hWnd, &rtWnd);

	//Graphics graphics(hdc); //mdc 생성

	//Graphics graphics_buffer(hdc); //tdc 생성

	//Image bufferImage(fullName); //loadImage SelectObject

	//Bitmap newImage(bufferImage.GetWidth(), bufferImage.GetHeight(), &graphics_buffer);
	//Rect rt;
	//rt.X = 0;
	//rt.Y = 0;
	//rt.Height = rtWnd.right;
	//rt.Width = rtWnd.bottom;
	//graphics.DrawImage(&newImage, rt, 0, 0, rtWnd.right, rtWnd.bottom, UnitPixel); //메모리 복사

	//벡버퍼 만듬
	Graphics backBuffer(hdc);
	Bitmap backBit(rtWnd.right, rtWnd.bottom);

	//메모리 버퍼
	Graphics gp(&backBit);

	//화면 지우기
	SolidBrush brs(Color(255, 0, 0, 255));
	gp.FillRectangle(&brs, 0, 0, rtWnd.right, rtWnd.bottom);

	//그리기
	Image image(fullName);
	gp.DrawImage(&image, posX, posY);
	
	//메인화면에 그리기
	backBuffer.DrawImage(&backBit, 0, 0);
}

void ImageDraw::DrawBMPToFileName(HINSTANCE hInst, HDC hdc, int posX, int posY, LPCWSTR fullName)
{
	BITMAP bit;
	HDC mdc = CreateCompatibleDC(hdc);
	HBITMAP hBit = (HBITMAP)LoadImage(hInst, fullName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	HBITMAP oldBit = (HBITMAP)SelectObject(mdc, hBit);
	GetObject(hBit, sizeof(BITMAP), &bit);
	BitBlt(hdc, posX, posY, bit.bmWidth, bit.bmHeight, mdc, 0, 0, SRCCOPY);
	SelectObject(mdc, oldBit);
	DeleteDC(mdc);
}

void ImageDraw::DrawBMPToFileName_DB(HINSTANCE hInst, HWND hWnd, HDC hdc, int posX, int posY, LPCWSTR fullName)
{
	RECT rtWnd;				//윈도우 크기를 받아오는 변수.
	HDC mdc, tdc;			//메모리 dc
	HBITMAP hBit, OldBit;	//hBit가 그림을 먼저 그려줄 BitMap
	HBITMAP newBit, oBit;
	BITMAP bit;

	//벡 버퍼 생성
	GetClientRect(hWnd, &rtWnd);
	mdc = CreateCompatibleDC(hdc);
	hBit = CreateCompatibleBitmap(hdc, rtWnd.right, rtWnd.bottom);
	OldBit = (HBITMAP)SelectObject(mdc, hBit);

	TextOut(mdc, 100, 10, fullName, lstrlen(fullName));
	//그림 그리기
	tdc = CreateCompatibleDC(hdc);
	newBit = (HBITMAP)LoadImage(hInst, fullName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	oBit = (HBITMAP)SelectObject(tdc, newBit);
	GetObject(newBit, sizeof(BITMAP), &bit);
	BitBlt(mdc, posX, posY, bit.bmWidth, bit.bmHeight, tdc, 0, 0, SRCCOPY);
	SelectObject(tdc, oBit);

	//화면 전체에 그림 복사
	BitBlt(hdc, 0, 0, rtWnd.right, rtWnd.bottom, mdc, 0, 0, SRCCOPY);

	//해제
	DeleteObject(newBit);
	DeleteObject(tdc);
	SelectObject(mdc, OldBit);
	DeleteObject(hBit);
	DeleteObject(newBit);
}
