#pragma once
#include "framework.h"

#include "ImageDraw.h"

#define MAX_LOADSTRING 100
class MyWindow
{
	HINSTANCE hInst;                                // 현재 인스턴스입니다.
	HWND hWnd;
	WCHAR szTitle[MAX_LOADSTRING];                  // 제목 표시줄 텍스트입니다.
	WCHAR szWindowClass[MAX_LOADSTRING];            // 기본 창 클래스 이름입니다.
	// 전역 변수:

public:
	static MyWindow* pMainWindow;
	MyWindow();
	BOOL InitInstance(HINSTANCE hInstance, int nCmdShow);
	BOOL InitChildInstance(HINSTANCE hInstance, int nCmdShow, LPCWSTR className, HWND parentHwnd, LPCWSTR caption);
	ATOM MyRegisterClass(HINSTANCE hInstance);
	ATOM MyRegisterClassCustom(HINSTANCE hInstance, LPCWSTR className);
	int MessageLoop();
	LRESULT MyWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
};

