﻿// 20200510.cpp : 애플리케이션에 대한 진입점을 정의합니다.
//

#include "framework.h"
#include "20200510.h"

#define MAX_LOADSTRING 100

// 전역 변수:
HINSTANCE hInst;                                // 현재 인스턴스입니다.
WCHAR szTitle[MAX_LOADSTRING];                  // 제목 표시줄 텍스트입니다.
WCHAR szWindowClass[MAX_LOADSTRING];            // 기본 창 클래스 이름입니다.

// 이 코드 모듈에 포함된 함수의 선언을 전달합니다:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

TCHAR _szBuffer[MAX_LOADSTRING] = { 0, };
TCHAR _szPopUpFileFullPath[MAX_LOADSTRING] = { 0, };
TCHAR _szSelectedFileFullPath[MAX_LOADSTRING] = { 0, };

HWND _lbFileListHwnd;
HWND _btnDrawHwnd;
HWND _btnDeleteHwnd;
HWND _txtFileNameHwnd;
HWND _sbHorHwnd;
HWND _sbVerHwnd;
HWND _chkIsDrawHwnd;

int maxScrollValue = 140;
int scrollTickValue = 20;
int scrollPageValue = 40;
int maxPieceCnt = 8;

int HorPieceCnt = 1;
int VerPieceCnt = 1;

bool isDraw = false;


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 여기에 코드를 입력합니다.

    // 전역 문자열을 초기화합니다.
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY20200510, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 애플리케이션 초기화를 수행합니다:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY20200510));

    MSG msg;

    // 기본 메시지 루프입니다:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  함수: MyRegisterClass()
//
//  용도: 창 클래스를 등록합니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY20200510));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MY20200510);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   용도: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   주석:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      50, 10, 1800, 1000, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  용도: 주 창의 메시지를 처리합니다.
//
//  WM_COMMAND  - 애플리케이션 메뉴를 처리합니다.
//  WM_PAINT    - 주 창을 그립니다.
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
        //컨트롤 생성
        _lbFileListHwnd = CreateWindow(L"ListBox", NULL, LBS_NOTIFY | WS_BORDER | WS_CHILD | WS_VISIBLE | WS_HSCROLL, 20, 20, 400, 600, hWnd, (HMENU)IDX_LBFILELIST, hInst, NULL);
        _btnDrawHwnd = CreateWindow(L"Button", L"Draw", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 20, 630, 80, 25, hWnd, (HMENU)IDX_BTNDRAW, hInst, NULL);
        _btnDeleteHwnd  = CreateWindow(L"Button", L"Delete", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 340, 630, 80, 25, hWnd, (HMENU)IDX_BTNDELETE, hInst, NULL);
        _txtFileNameHwnd  = CreateWindow(L"Edit", L"FileName", WS_BORDER | WS_CHILD | WS_VISIBLE, 460, 20, 200, 25, hWnd, (HMENU)IDX_TXTFILENAME, hInst, NULL);
        _chkIsDrawHwnd = CreateWindow(L"Button", L"IsDraw", WS_CHILD | WS_VISIBLE | BS_CHECKBOX, 460, 65, 100, 25, hWnd, (HMENU)IDX_CHKISDRAW, hInst, NULL);
        _sbHorHwnd = CreateWindow(L"ScrollBar", NULL, WS_CHILD | WS_VISIBLE | SBS_HORZ, 460, 560, 600, 20, hWnd, (HMENU)IDX_SBHOR, hInst, NULL);
        _sbVerHwnd = CreateWindow(L"ScrollBar", NULL, WS_CHILD | WS_VISIBLE | SBS_VERT, 1060, 110, 20, 450, hWnd, (HMENU)IDX_SBVER, hInst, NULL);

        SendMessage(_lbFileListHwnd, LB_SETHORIZONTALEXTENT, 500, NULL); //리스트박스 스크롤바 limit 설정
        SetScrollRange(_sbHorHwnd, SB_CTL, 0, maxScrollValue, true);
        SetScrollRange(_sbVerHwnd, SB_CTL, 0, maxScrollValue, true);
        break;
    case WM_COMMAND:
    {
        int wmId = LOWORD(wParam);
        // 메뉴 선택을 구문 분석합니다:
        switch (wmId)
        {
        case IDM_OPENFILE_DLG:
        {

            TCHAR dir[MAX_PATH] = { 0, };
            _tgetcwd(dir, MAX_PATH);    // 최근 열었던 파일경로 default 셋팅
            OPENFILENAME ofn;
            memset(&ofn, 0, sizeof(OPENFILENAME));
            ofn.lStructSize = sizeof(OPENFILENAME);
            ofn.hwndOwner = hWnd;
            ofn.lpstrFile = _szPopUpFileFullPath;
            ofn.lpstrFilter = L"BMP 파일\0*.bmp;*.BMP\0모든파일\0*.*\0";
            ofn.nMaxFile = 100;
            ofn.lpstrInitialDir = dir;
            if (GetOpenFileName(&ofn) != false)
            {
                //listbox에 추가
                SendMessage(_lbFileListHwnd, LB_ADDSTRING, 0, (LPARAM)_szPopUpFileFullPath);
            }

            break;
        }
        case IDM_ABOUT:
            DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
            break;
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
        case IDX_LBFILELIST:
            switch (HIWORD(wParam))
            {
            case LBN_SELCHANGE:
            {
                int num = SendMessage(_lbFileListHwnd, LB_GETCURSEL, 0, 0);
                SendMessage(_lbFileListHwnd, LB_GETTEXT, num, (LPARAM)_szSelectedFileFullPath);
            }
            break;
            default:
                break;
            }
            break;
        case IDX_BTNDRAW:
            lstrcpy(_szBuffer, _szSelectedFileFullPath);
            if (lstrlen( _szSelectedFileFullPath) > 0)
            {
                SetWindowText(_txtFileNameHwnd, (wcsrchr(_szSelectedFileFullPath, '\\') + 1));
                SendMessage(_chkIsDrawHwnd, BM_SETCHECK, BST_CHECKED, 0);
                isDraw = true;
            }
            InvalidateRect(hWnd, NULL, true);
            break;
        case IDX_BTNDELETE:{
            int num = SendMessage(_lbFileListHwnd, LB_GETCURSEL, 0, 0);
            SendMessage(_lbFileListHwnd, LB_DELETESTRING, num, NULL);
            memset(_szSelectedFileFullPath, 0, sizeof(_szSelectedFileFullPath));
        }
            break;
        case IDX_CHKISDRAW:
            if (SendMessage(_chkIsDrawHwnd, BM_GETCHECK, 0, 0) == BST_UNCHECKED)
            {
                SendMessage(_chkIsDrawHwnd, BM_SETCHECK, BST_CHECKED, 0);
                isDraw = true;
            }
            else {
                SendMessage(_chkIsDrawHwnd, BM_SETCHECK, BST_UNCHECKED, 0);
                isDraw = false;
            }
            break;

        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    case WM_VSCROLL: {
        int vScrollValue = GetScrollPos(_sbVerHwnd, SB_CTL);
        switch (wParam) {
        case SB_LINEUP:
            vScrollValue = max(0, vScrollValue - scrollTickValue);
            break;
        case SB_LINEDOWN:
            vScrollValue = min(140, vScrollValue + scrollTickValue);
            break;
        case SB_PAGEUP:
            vScrollValue = max(0, vScrollValue - scrollPageValue);
            break;
        case SB_PAGEDOWN:
            vScrollValue = min(140, vScrollValue + scrollPageValue);
            break;
        case SB_THUMBTRACK:
            vScrollValue = HIWORD(wParam);
            break;
        default:
            break;
        }
        VerPieceCnt = (float)vScrollValue / maxScrollValue * (maxPieceCnt-1) + 1;
        SetScrollPos(_sbVerHwnd, SB_CTL, vScrollValue, true);
        InvalidateRect(hWnd, NULL, true);
    }
        break;
    case WM_HSCROLL: {
        int hScrollValue = GetScrollPos(_sbHorHwnd, SB_CTL);
        switch (wParam) {
        case SB_LINEUP:
            hScrollValue = max(0, hScrollValue - scrollTickValue);
            break;
        case SB_LINEDOWN:
            hScrollValue = min(140, hScrollValue + scrollTickValue);
            break;
        case SB_PAGEUP:
            hScrollValue = max(0, hScrollValue - scrollPageValue);
            break;
        case SB_PAGEDOWN:
            hScrollValue = min(140, hScrollValue + scrollPageValue);
            break;
        case SB_THUMBTRACK:
            hScrollValue = HIWORD(wParam);
        default:
            break;
        }
        HorPieceCnt = (float)hScrollValue / maxScrollValue * (maxPieceCnt - 1) + 1;
        SetScrollPos(_sbHorHwnd, SB_CTL, hScrollValue, true);
        InvalidateRect(hWnd, NULL, true);
    }
                   break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다...

            //그림 그리기
            if (isDraw == true)
            {
                HDC mdc;
                mdc = CreateCompatibleDC(hdc);

                HBITMAP oldBit;
                BITMAP bit;
                HBITMAP hBit = (HBITMAP)LoadImage(NULL, _szBuffer, IMAGE_BITMAP, 600, 450, LR_LOADFROMFILE);
                oldBit = (HBITMAP)SelectObject(mdc, hBit);
                GetObject(hBit, sizeof(bit), &bit);
                int width = bit.bmWidth;
                int height = bit.bmHeight;

                int onepieceWidth = width / HorPieceCnt;
                int onepieceHeight = height / VerPieceCnt;

                for (int i = 0; i < HorPieceCnt; i++)
                {
                    for (int j = 0; j < VerPieceCnt; j++)
                    {
                        StretchBlt(hdc, 460 + (onepieceWidth+3) * i, 110 + (onepieceHeight+3) * j, onepieceWidth, onepieceHeight, mdc, (onepieceWidth+3) * i, (onepieceHeight+3)* j, onepieceWidth - 3, onepieceHeight - 3, SRCCOPY);
                    }
                }
                SelectObject(mdc, oldBit);
                DeleteObject(hBit);
            }

            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
